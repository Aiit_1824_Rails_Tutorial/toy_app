Rails.application.routes.draw do
  resources :microposts
  resources :users
  
  # /users => users#index
  # /users/new => users#new
  # ...
  
  #root 'application#hello'
  root 'users#index'
end
